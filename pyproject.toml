[project]
name = "pve-cli"
version = "0.7.0"
description = "CLI Tool to manage VMs and more on proxmox clusters"
authors = [
    {name = "Dominik Rimpf", email = "dev@drimpf.de"}
]
readme = "README.md"
requires-python = ">=3.9"
dependencies = [
    "proxmoxer~=2.0",
    "requests~=2.31",
    "typer[all]~=0.11",
    "toml~=0.10",
    "requests-toolbelt~=1.0",
]

classifiers = [
    "Development Status :: 5 - Production/Stable",
    "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
]

[project.urls]
"Source code" = "https://gitlab.com/domrim/pve-cli"

[project.scripts]
pve-cli = "pve_cli.main:cli"

[tool.pdm.scripts]
ci-bandit = "bandit -c pyproject.toml -r ."
ci-mypy = {shell = "mypy pve_cli --no-error-summary | mypy-gitlab-code-quality > gl-code-quality-report.json"}
ci-ruff = {shell = "ruff check --output-format=gitlab | tee gl-code-quality-report.json"}
ci-pytest = "pytest --cov --cov-report term --cov-report xml:coverage.xml"

[tool.pdm.dev-dependencies]
bandit = [
    "bandit[toml]~=1.7",
]
mypy = [
    "mypy~=1.9",
    "types-toml~=0.10",
    "types-requests~=2.31",
    "mypy-gitlab-code-quality~=1.1",
]
ruff = [
    "ruff~=0.6",
]
test = [
    "pytest~=8.1",
    "pytest-cov~=5.0",
]


[tool.ruff]
# 80 chars aren't enough in 21 century
line-length = 128

show-fixes = true
fix = false

[tool.ruff.lint]
select = ["F", "E", "W", "C90", "I", "N", "UP", "ANN", "S", "B", "A", "C4", "G", "PIE", "T20", "PYI", "PT", "Q", "RET", "SIM", "PTH", "RUF"]
# ANN101,ANN102: flake8-annotations, anotations for self and cls
# S101: flake8-bandit forbids asserts
# UP007: ignore modern typehints until https://github.com/tiangolo/typer/issues/533 is fixed
ignore = ["ANN101", "ANN102", "S101", "UP007"]

[tool.ruff.lint.isort]
relative-imports-order = "closest-to-furthest"

[tool.ruff.lint.flake8-annotations]
# Ignore opinionated flake8-annotation rule
suppress-none-returning = true

[tool.ruff.lint.flake8-quotes]
inline-quotes = "single"

[tool.ruff.format]
# 5. Use single quotes for non-triple-quoted strings.
quote-style = "single"

[tool.mypy]
python_version = "3.11"
warn_return_any = true
warn_unused_configs = true

[[tool.mypy.overrides]]
module = [
    "proxmoxer.*",
]
ignore_missing_imports = true

[tool.bandit]
exclude_dirs = ["tests", ".venv"]

[build-system]
requires = ["pdm-backend"]
build-backend = "pdm.backend"
