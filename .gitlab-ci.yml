---
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  GIT_DEPTH: 1

default:
  image: docker.io/library/python:3.11
  cache:
    paths:
      - ".cache/pip"
  before_script:
    - command -v pdm || pip3 install pdm

stages:
  - lint
  - test
  - build
  - release

ruff:
  stage: lint
  script:
    - pdm install -dG ruff
    - pdm run ci-ruff
  artifacts:
    when: always
    reports:
      codequality: "${CI_PROJECT_DIR}/gl-code-quality-report.json"

mypy:
  stage: lint
  script:
    - pdm install -dG mypy
    - pdm run ci-mypy
  artifacts:
    when: always
    reports:
      codequality: gl-code-quality-report.json

bandit:
  stage: lint
  script:
    - pdm install -dG bandit
    - pdm run ci-bandit

.pytest:
  stage: test
  script:
    - pdm install -dG test
    - pdm run ci-pytest
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

pytest-python3.9:
  extends: .pytest
  image: docker.io/library/python:3.9

pytest-python3.10:
  extends: .pytest
  image: docker.io/library/python:3.10

pytest-python3.11:
  extends: .pytest
  image: docker.io/library/python:3.11

pytest-python3.12:
  extends: .pytest
  image: docker.io/library/python:3.12

build:
  stage: build
  before_script:
    - pip3 install -U build
  script:
    - python3 -m build
  artifacts:
    paths:
      - dist/*
    expire_in: 8 hours

check-tag-in-default-branch:
  stage: release
  before_script:
    - git fetch origin $CI_DEFAULT_BRANCH:$CI_DEFAULT_BRANCH
  script:
    - git merge-base --is-ancestor $CI_COMMIT_TAG $CI_DEFAULT_BRANCH
  variables:
    GIT_DEPTH: 0
  # run only on tagged semver commits
  rules:
    - if: $CI_COMMIT_TAG =~ /^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/


release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  before_script: []
  script:
    - echo "Release"
  release:
    tag_name: "${CI_COMMIT_TAG}"
    name: "${CI_PROJECT_NAME} ${CI_COMMIT_TAG}"
    description: |
      Package is available on PyPI: https://pypi.org/project/pve-cli/
  needs:
    - job: check-tag-in-default-branch
    - job: build
      artifacts: True
  # run only on tagged semver commits
  rules:
    - if: $CI_COMMIT_TAG =~ /^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/

upload:
  stage: release
  before_script:
    - pip3 install -U twine
  script:
    - python3 -m twine upload --verbose dist/*
  variables:
    TWINE_NON_INTERACTIVE: "true"
  needs:
    - job: check-tag-in-default-branch
    - job: build
      artifacts: True
  # run only on tagged semver commits
  rules:
    - if: $CI_COMMIT_TAG =~ /^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/
